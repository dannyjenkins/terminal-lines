public class TransformationMatrix {

    private double topLeft;
    private double topRight;
    private double bottomLeft;
    private double bottomRight;
    private Location center;

    public TransformationMatrix(
            double topLeft,
            double topRight,
            double bottomLeft,
            double bottomRight,
            double centerX,
            double centerY
    ) {
        this.topLeft     = topLeft;
        this.topRight    = topRight;
        this.bottomLeft  = bottomLeft;
        this.bottomRight = bottomRight;
        this.center      = new Location(centerX, centerY);
    }

    public TransformationMatrix(
            double topLeft,
            double topRight,
            double bottomLeft,
            double bottomRight,
            Location center
    ) {
        this(topLeft, topRight, bottomLeft, bottomRight, center.getX(), center.getY());
    }

    public double getTopLeft() {
        return topLeft;
    }

    public double getTopRight() {
        return topRight;
    }

    public double getBottomLeft() {
        return bottomLeft;
    }

    public double getBottomRight() {
        return bottomRight;
    }

    public Location getCenter() {
        return center;
    }

}
