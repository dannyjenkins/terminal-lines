import java.util.ArrayList;

public class Rectangle implements IShape{

    private Surface surface;
    private ArrayList<LineSegment> edges;
    private ArrayList<Vertex> vertices;
    private int bottomLeftX, bottomLeftY, width, height;
    private Color color;
    private Location center;

    /**
     * Constructor for a <code>Rectangle</code>.
     * @param surface The surface on which the <code>Rectangle</code> should be drawn
     * @param bottomLeftX The value on the X-axis of the bottom left vertex in the <code>Rectangle</code>
     * @param bottomLeftY The value on the Y-axis of the bottom left vertex in the <code>Rectangle</code>
     * @param width The width of the <code>Rectangle</code>
     * @param height The width of the <code>Rectangle</code>
     * @param color The color of the <code>Rectangle</code>
     */
    public Rectangle(Surface surface, int bottomLeftX, int bottomLeftY, int width, int height, Color color) {
        this.surface     = surface;
        this.bottomLeftX = bottomLeftX;
        this.bottomLeftY = bottomLeftY;
        this.width       = width;
        this.height      = height;
        this.edges       = new ArrayList<>();
        this.vertices    = new ArrayList<>();
        this.color       = color;
        this.center      = new Location(bottomLeftX + ((double) width / 2), bottomLeftY + ((double) height / 2));

        // bottom left, bottom right, top left, top right
        this.vertices.add(new Vertex(surface, surface.getLocationAt(bottomLeftX, bottomLeftY), color));
        this.vertices.add(new Vertex(surface, surface.getLocationAt(bottomLeftX + width, bottomLeftY), color));
        this.vertices.add(new Vertex(surface, surface.getLocationAt(bottomLeftX, bottomLeftY + height), color));
        this.vertices.add(new Vertex(surface, surface.getLocationAt(bottomLeftX + width, bottomLeftY + height), color));

        // bottom, left, right, top
        this.edges.add(new LineSegment(surface, vertices.get(0), vertices.get(1), color));
        this.edges.add(new LineSegment(surface, vertices.get(0), vertices.get(2), color));
        this.edges.add(new LineSegment(surface, vertices.get(1), vertices.get(3), color));
        this.edges.add(new LineSegment(surface, vertices.get(2), vertices.get(3), color));
    }

    /**
     * Constructor for a <code>Rectangle</code>, without a color argument.
     * The <code>Rectangle</code> will thus be colored the default text color of the terminal.
     * @param surface The surface on which the <code>Rectangle</code> should be drawn
     * @param bottomLeftX The value on the X-axis of the bottom left vertex in the <code>Rectangle</code>
     * @param bottomLeftY The value on the Y-axis of the bottom left vertex in the <code>Rectangle</code>
     * @param width The width of the <code>Rectangle</code>
     * @param height The width of the <code>Rectangle</code>
     */
    public Rectangle(Surface surface, int bottomLeftX, int bottomLeftY, int width, int height) {
        this(surface, bottomLeftX, bottomLeftY, width, height, Color.WHITE);
    }

    @Override
    public int numOfEdges() {
        return edges.size();
    }

    @Override
    public int numOfVertices() {
        return vertices.size();
    }

    @Override
    public double area() {
        return width * height;
    }

    @Override
    public Vertex getVertex(int index) {
        return vertices.get(index);
    }

    @Override
    public LineSegment getEdge(int index) {
        return edges.get(index);
    }

    public Location getCenter() {
        return center;
    }

    public Location getTopLeftInRelationToCenter() {
        return new Location(
                bottomLeftX          - getCenter().getX(),
                bottomLeftY + height - getCenter().getY()
        );
    }

    public Location getTopRightInRelationToCenter() {
        return new Location(
                bottomLeftX + width  - getCenter().getX(),
                bottomLeftY + height - getCenter().getY()
        );
    }

    public Location getBottomLeftInRelationToCenter() {
        return new Location(
                bottomLeftX - getCenter().getX(),
                bottomLeftY - getCenter().getY()
        );
    }
    public Location getBottomRightInRelationToCenter() {
        return new Location(
                bottomLeftX + width - getCenter().getX(),
                bottomLeftY         - getCenter().getY()
        );
    }

    public void transform(TransformationMatrix t) {

        double centerX = t.getCenter().getX();
        double centerY = t.getCenter().getY();

        double oldBottomLeftX  = getVertex(0).getX();
        double oldBottomLeftY  = getVertex(0).getY();
        double oldBottomRightX = getVertex(1).getX();
        double oldBottomRightY = getVertex(1).getY();
        double oldTopLeftX     = getVertex(2).getX();
        double oldTopLeftY     = getVertex(2).getY();
        double oldTopRightX    = getVertex(3).getX();
        double oldTopRightY    = getVertex(3).getY();

        double oldBLXRelativeToCenter = centerX - oldBottomLeftX;
        double oldBLYRelativeToCenter = centerY - oldBottomLeftY;
        double oldBRXRelativeToCenter = centerX - oldBottomRightX;
        double oldBRYRelativeToCenter = centerY - oldBottomRightY;
        double oldTLXRelativeToCenter = centerX - oldTopLeftX;
        double oldTLYRelativeToCenter = centerY - oldTopLeftY;
        double oldTRXRelativeToCenter = centerX - oldTopRightX;
        double oldTRYRelativeToCenter = centerY - oldTopRightY;

        double newBLX = t.getTopLeft()    * oldBLXRelativeToCenter + t.getTopRight()    * oldBLYRelativeToCenter;
        double newBLY = t.getBottomLeft() * oldBLXRelativeToCenter + t.getBottomRight() * oldBLYRelativeToCenter;
        double newBRX = t.getTopLeft()    * oldBRXRelativeToCenter + t.getTopRight()    * oldBRYRelativeToCenter;
        double newBRY = t.getBottomLeft() * oldBRXRelativeToCenter + t.getBottomRight() * oldBRYRelativeToCenter;
        double newTLX = t.getTopLeft()    * oldTLXRelativeToCenter + t.getTopRight()    * oldTLYRelativeToCenter;
        double newTLY = t.getBottomLeft() * oldTLXRelativeToCenter + t.getBottomRight() * oldTLYRelativeToCenter;
        double newTRX = t.getTopLeft()    * oldTRXRelativeToCenter + t.getTopRight()    * oldTRYRelativeToCenter;
        double newTRY = t.getBottomLeft() * oldTRXRelativeToCenter + t.getBottomRight() * oldTRYRelativeToCenter;

        newBLX += centerX;
        newBLY += centerY;
        newBRX += centerX;
        newBRY += centerY;
        newTLX += centerX;
        newTLY += centerY;
        newTRX += centerX;
        newTRY += centerY;

        getVertex(0).setLocation(new Location(newBLX, newBLY));
        getVertex(1).setLocation(new Location(newBRX, newBRY));
        getVertex(2).setLocation(new Location(newTLX, newTLY));
        getVertex(3).setLocation(new Location(newTRX, newTRY));
    }

    public void rotate(double theta) {

        double thetaRadians = Math.toRadians(theta);

        TransformationMatrix t = new TransformationMatrix(
                Math.cos(thetaRadians),
                -Math.sin(thetaRadians),
                Math.sin(thetaRadians),
                Math.cos(thetaRadians),
                getCenter()
        );

        transform(t);

    }
}
