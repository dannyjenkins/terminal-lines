import java.util.ArrayList;

public class Surface {


    /**
     * The width and height of the <code>Surface</code>.
     */
    private final int width, height;

    /**
     * A two-dimensional array of the possible locations on the <code>Surface</code>.
     */
    private Location[][] fields;

    /**
     * ArrayList of the line segments on the <code>Surface</code>.
     */
    private ArrayList<LineSegment> lines;

    /**
     * ArrayList of the vertices on the <code>Surface</code>.
     */
    private ArrayList<Vertex> vertices;

    /**
     * When drawn, this symbol will be used to represent a vertex.
     */
    private char vertexSymbol = '*';

    /**
     * When drawn, this symbol will be used to represent a line segment.
     */
    private char lineSegmentSymbol = '\u00b7';

    /**
     * Use two of the symbols in a row, to take on more of a square look.
     */
    private String vertexString      = String.valueOf(vertexSymbol).repeat(2);
    private String lineSegmentString = String.valueOf(lineSegmentSymbol).repeat(2);

    public Surface(int width, int height) {
        this.width    = width;
        this.height   = height;
        this.fields   = new Location[height][width];
        this.lines    = new ArrayList<>();
        this.vertices = new ArrayList<>();

        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                fields[h][w] = new Location(w, h);
            }
        }
    }

    public Location[][] getFields() {
        return fields;
    }

    public ArrayList<LineSegment> getLines() {
        return lines;
    }

    public void addLine(LineSegment line) {
        lines.add(line);
    }

    public void addVertex(Vertex vertex) {
        vertices.add(vertex);
        getLocationAt(vertex.getX(), vertex.getY()).addVertex(vertex);
    }

    public void removeVertex(Vertex vertex) {
        vertices.remove(vertex);
        getLocationAt(vertex.getX(), vertex.getY()).removeVertex(vertex);
    }

    public Location getLocationAt(double width, double height) {
        int widthInt  = (int) Math.round(width);
        int heightInt = (int) Math.round(height);
        return fields[heightInt][widthInt];
    }

    public Vertex getVertexAt(int width, int height) {
        Location loc = getLocationAt(width, height);
        return loc.getVertex(loc.vertexCount() - 1);
    }


     public LineSegment getLineSegmentAt(int x, int y) {
        for (int i = getLines().size() - 1; i >= 0; i--) {
            if (getLines().get(i).hitsLine(getLocationAt(x, y))) {
                return getLines().get(i);
            }
        }
        return null;
    }

    private void drawCoordinate(int width, int height) {
        if (getVertexAt(width, height) != null && getVertexAt(width, height).isVisible())
            System.out.print(
                    Color.getString(getVertexAt(width, height).getColor())
                            + vertexString
                            + Color.getString(Color.RESET));

        else if (getLineSegmentAt(width, height) != null)
            System.out.print(
                    Color.getString(getLineSegmentAt(width, height).getColor())
                            + lineSegmentString
                            + Color.getString(Color.RESET));

        else
            System.out.print("  ");
    }

    public void draw() {
        String topAndBottom = "--".repeat(width + 1);
        System.out.println(topAndBottom);

        for (int h = height - 1; h >= 0; h--) {
            System.out.print('|');
            for (int w = 0; w < width; w++) {
                drawCoordinate(w, h);
            }
            System.out.println('|');
        }

        System.out.println(topAndBottom);
    }
}
