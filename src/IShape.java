public interface IShape {

    /**
     * @return the number of edges in the shape.
     */
    public int numOfEdges();

    /**
     * @return the number of vertices in the shape.
     */
    public int numOfVertices();

    /**
     * @return the shape's area/size.
     */
    public double area();

    /**
     * @param index the index of the <code>Vertex</code> to get
     */
    public Vertex getVertex(int index);

    /**
     * @param index the index of the <code>LineSegment</code> to get
     * @return the <code>LineSegment</code> at that index
     */
    public LineSegment getEdge(int index);

    /**
     * Perform a transformation on the shape.
     * @param t The transformation matrix with which to transform the shape
     */
    public void transform(TransformationMatrix t);

    /**
     * Rotate the shape by an angle theta.
     * @param theta The angle by which to rotate the shape, in degrees
     */
    public void rotate(double theta);
}
