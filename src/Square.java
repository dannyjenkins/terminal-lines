public class Square extends Rectangle {

    public Square(Surface surface, int bottomLeftX, int bottomLeftY, int sideLength) {
        super(surface, bottomLeftX, bottomLeftY, sideLength, sideLength);
    }

    public Square(Surface surface, int bottomLeftX, int bottomLeftY, int sideLength, Color color) {
        super(surface, bottomLeftX, bottomLeftY, sideLength, sideLength, color);
    }

}
