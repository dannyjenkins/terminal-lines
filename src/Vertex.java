public class Vertex {

    private Location loc;
    private Surface surface;
    private Color color;
    private boolean visible;

    /**
     * @param visible Whether the <code>Vertex</code> is visible.
     * @param s The parent <code>Surface</code>.
     * @param loc The <code>Location</code> of the <code>Vertex</code>.
     * @param color The color of the <code>Vertex</code>.
     */
    public Vertex(boolean visible, Surface s, Location loc, Color color) {
        this.visible = visible;
        this.surface = s;
        this.loc     = loc;
        this.color   = color;
        s.addVertex(this);
    }

    /**
     * <code>Vertex</code> constructor where the color defaults to white.
     * @param visible Whether the <code>Vertex</code> is visible.
     * @param s The parent <code>Surface</code>.
     * @param loc The <code>Location</code> of the <code>Vertex</code>.
     */
    public Vertex(boolean visible, Surface s, Location loc) {
        this(visible, s, loc, Color.WHITE);
    }

    /**
     * <code>Vertex</code> constructor where the visibility defaults to true, and the color defaults to white.
     * @param s The parent <code>Surface</code>.
     * @param loc The <code>Location</code> of the <code>Vertex</code>.
     */
    public Vertex(Surface s, Location loc) {
        this(true, s, loc, Color.WHITE);
    }

    /**
     * <code>Vertex</code> constructor where the visibility defaults to true.
     * @param s The parent <code>Surface</code>.
     * @param loc The <code>Location</code> of the <code>Vertex</code>.
     * @param color The color of the <code>Vertex</code>.
     */
    public Vertex(Surface s, Location loc, Color color) {
        this(true, s, loc, color);
    }

    public double getX() {
        return loc.getX();
    }

    public double getY() {
        return loc.getY();
    }

    public Color getColor() {
        return color;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean value) {
        visible = value;
    }

    public void setLocation(Location newLoc) {
        surface.removeVertex(this);
        this.loc = newLoc;
        surface.addVertex(this);
    }
}
