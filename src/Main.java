import java.io.IOException;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        rotationTest();
    }

    public static void oneLineTest() {
        Surface s = new Surface(10, 15);
        LineSegment ls1 = new LineSegment(s, 6,  13, (6  + 0),  1, Color.RED);
        s.draw();
    }

    public static void lineTest() {
        Surface s = new Surface(50, 15);

        LineSegment ls1 = new LineSegment(s, 1,  13, (1  + 0),  1, Color.RED);
        LineSegment ls2 = new LineSegment(s, 3,  13, (3  + 3),  1, Color.GREEN);
        LineSegment ls3 = new LineSegment(s, 5,  13, (5  + 6),  1, Color.YELLOW);
        LineSegment ls4 = new LineSegment(s, 7,  13, (7  + 9),  1, Color.BLUE);
        LineSegment ls5 = new LineSegment(s, 9,  13, (9  + 12), 1, Color.PURPLE);
        LineSegment ls6 = new LineSegment(s, 11, 13, (11 + 15), 1, Color.CYAN);
        LineSegment ls7 = new LineSegment(s, 28, 1,  (28 + 20), 1);

        s.draw();

        System.out.println("1: " + ls1.slope());
        System.out.println("2: " + ls2.slope());
        System.out.println("3: " + ls3.slope());
        System.out.println("4: " + ls4.slope());
        System.out.println("5: " + ls5.slope());
        System.out.println("6: " + ls6.slope());
        System.out.println("7: " + ls7.slope());
    }
    public static void transformationTest() {
        Surface s = new Surface(50, 20);
        Square square = new Square(s, 20, 3, 10, Color.BLUE);

        s.draw();

        square.transform(new TransformationMatrix(
                0,
                -1,
                1,
                0,
                square.getCenter()));

        s.draw();
    }

    public static void rotationTest() throws InterruptedException {
        Surface s = new Surface(80, 50);
        Square square = new Square(s, 20, 15, 25, Color.BLUE);

        System.out.println("\u001b[?25l]");

        for (int i = 0; i <= 360; i++) {
            System.out.print("\033[H\0332J");
            s.draw();
            Thread.sleep(10);
            square.rotate(1);
        }

        System.out.println("\u001b[?25h]");

    }
}