public class LineSegment {

    /**
     * The vertices between which the <code>LineSegment</code> should be.
     */
    private Vertex first, second;

    /**
     * The parent surface on which the <code>LineSegment</code> should be.
     */
    private Surface surface;

    /**
     * The line color.
     */
    private Color color;

    /**
     * A <code>LineSegment</code> constructor with a color argument.
     * @param surface The parent surface on which the <code>LineSegment</code> should be
     * @param first The first <code>Vertex</code>
     * @param second The second <code>Vertex</code>
     * @param color The color of the line
     */
    public LineSegment(Surface surface, Vertex first, Vertex second, Color color) {
        this.surface = surface;
        this.first   = first;
        this.second  = second;
        this.color   = color;
        if (isAPoint())
            throw new IllegalArgumentException("Both vertices of a line cannot occupy the same coordinate.");
        surface.addLine(this);
    }

    /**
     * A <code>LineSegment</code> constructor without a color argument. It will use the default color, which is normal
     * terminal text color.
     * @param surface The parent surface on which the <code>LineSegment</code> should be
     * @param first The first <code>Vertex</code>
     * @param second The second <code>Vertex</code>
     */
    public LineSegment(Surface surface, Vertex first, Vertex second) {
        this(surface, first, second, Color.WHITE);
    }

    /**
     * Constructor based on double coordinate values, rather than concrete <code>Vertex</code> objects.
     * @param surface The parent surface on which the <code>LineSegment</code> should be
     * @param firstX The X-value of the first <code>Vertex</code>.
     * @param firstY The Y-value of the first <code>Vertex</code>.
     * @param secondX The X-value of the second <code>Vertex</code>.
     * @param secondY The Y-value of the second <code>Vertex</code>.
     * @param color The color of the line
     */
    public LineSegment(Surface surface, double firstX, double firstY, double secondX, double secondY, Color color) {
        this.surface = surface;
        this.first   = new Vertex(surface, new Location(firstX, firstY), color);
        this.second  = new Vertex(surface, new Location(secondX, secondY), color);
        this.color   = color;
        if (isAPoint())
            throw new IllegalArgumentException("Both vertices of a line cannot occupy the same coordinate.");
        surface.addLine(this);
    }

    /**
     * Constructor based on double coordinate values, rather than concrete <code>Vertex</code> objects.
     * @param surface The parent surface on which the <code>LineSegment</code> should be
     * @param firstX The X-value of the first <code>Vertex</code>.
     * @param firstY The Y-value of the first <code>Vertex</code>.
     * @param secondX The X-value of the second <code>Vertex</code>.
     * @param secondY The Y-value of the second <code>Vertex</code>.
     */
    public LineSegment(Surface surface, double firstX, double firstY, double secondX, double secondY) {
        this(surface, firstX, firstY, secondX, secondY, Color.WHITE);
    }

    /**
     * @return The first connecting <code>Vertex</code>.
     */
    public Vertex getFirst() {
        return first;
    }

    /**
     * @return The second connecting <code>Vertex</code>.
     */
    public Vertex getSecond() {
        return second;
    }

    /**
     * @return The color of the <code>LineSegment</code>.
     */
    public Color getColor() {
        return color;
    }

    /**
     * @return the distance along the x-axis from the first <code>Vertex</code> to the second <code>Vertex</code>.
     */
    private double deltaX() {
        return second.getX() - first.getX();
    }

    /**
     * @return the distance along the y-axis from the first <code>Vertex</code> to the second <code>Vertex</code>.
     */
    private double deltaY() {
        return second.getY() - first.getY();
    }

    /**
     * @return the slope of the <code>LineSegment</code>.
     */
    public double slope() {
        return deltaY() / deltaX();
    }

    /**
     * @return Whether the <code>LineSegment</code> is completely horizontal.
     */
    public boolean isHorizontal() {
        return first.getY() == second.getY();
    }

    /**
     * @return Whether the <code>LineSegment</code> is completely vertical.
     */
    public boolean isVertical() {
        return first.getX() == second.getX();
    }

    /**
     * @return Whether the <code>LineSegment</code> is a point, i.e. both vertices of the line occupy the same
     * coordinate.
     */
    private boolean isAPoint() {
        return isHorizontal() && isVertical();
    }

    /**
     * @return Whether the <code>LineSegment</code> is steeper than 45 degrees, or whether the line is more vertical
     * than it is horizontal.
     */
    public boolean isSteeperThan45Degrees() {
        if (isVertical()) return true;
        return slope() > 1.0 || slope() < -1.0;
    }

    /**
     * @param l A given location
     * @return whether this location resides within the X range of the line, i.e. is between the X values of the line's
     * two vertices.
     */
    private boolean withinXRangeOfLine(Location l) {
        return first.getX() < l.getX() && l.getX() < second.getX() ||
                second.getX() < l.getX() && l.getX() < first.getX();
    }

    /**
     * @param l A given location
     * @return whether this location resides within the Y range of the line, i.e. is between the Y values of the line's
     * two vertices.
     */
    private boolean withinYRangeOfLine(Location l) {
        return first.getY() < l.getY() && l.getY() < second.getY() ||
                second.getY() < l.getY() && l.getY() < first.getY();
    }

    /**
     * @param given A given location
     * @return whether this given location hits this line (occupies part of this line's space).
     */
    public boolean hitsLine(Location given) {
        // what to do if the line is vertical
        if (isVertical()) {
            return given.getX() == first.getX() && withinYRangeOfLine(given);
        }

        else {
            if (withinXRangeOfLine(given) && !isSteeperThan45Degrees())
                return given.getY() == yValueInt(given.getXRounded());
            else if (withinYRangeOfLine(given) && isSteeperThan45Degrees()) {
                return given.getX() == xValueInt(given.getYRounded());
            }
        }

        return false;
    }

    /**
     * @return the value on the Y-axis we would get if we were to lengthen the <code>LineSegment</code> until it reaches
     * the Y-axis.
     */
    public double valueAtYAxis() {
        return first.getY() + (slope() * -(first.getX()));
    }

    /**
     * @param yValue a given value on the Y-axis
     * @return the value on the X-axis for the given <code>yValue</code>.
     */
    public double xValue(int yValue) {
        return (yValue - valueAtYAxis()) / slope();
    }

    /**
     * @param yValue a given value on the Y-axis
     * @return the value on the X-axis for the given <code>yValue</code>, rounded to the nearest integer.
     */
    public int xValueInt(int yValue) {
        return (int) Math.round(xValue(yValue));
    }

    /**
     * @param xValue a given value on the X-axis
     * @return the value on the Y-axis for the given <code>xValue</code>.
     */
    public double yValue(int xValue) {
        return (slope() * xValue) + valueAtYAxis();
    }


    /**
     * @param xValue a given value on the X-axis
     * @return the value on the Y-axis for the given <code>xValue</code>, rounded to the nearest integer.
     */
    public int yValueInt(int xValue) {
        return (int) Math.round(yValue(xValue));
    }
}
