public enum Color {
    WHITE,
    BLACK,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    PURPLE,
    CYAN,
    RESET;

    public static String getString(Color c) {
        return switch (c) {
            case WHITE  -> "";
            case BLACK  -> "\033[0;30m";
            case RED    -> "\033[0;31m";
            case GREEN  -> "\033[0;32m";
            case YELLOW -> "\033[0;33m";
            case BLUE   -> "\033[0;34m";
            case PURPLE -> "\033[0;35m";
            case CYAN   -> "\033[0;36m";
            case RESET  -> "\033[0m";
        };
    }
}
