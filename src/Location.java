import java.util.ArrayList;

public class Location {

    private double x, y;
    private ArrayList<Vertex> vertices;

    public Location(double x, double y) {
        this.x = x;
        this.y = y;
        this.vertices = new ArrayList<>();
    }

    public String toString() {
        return "(" + Double.toString(x) + ", " + Double.toString(y) + ")";
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getXRounded() {
        return (int) Math.round(x);
    }

    public int getYRounded() {
        return (int) Math.round(y);
    }

    /**
     * @return the number of vertices that occupy this <code>Location</code>.
     */
    public int vertexCount() {
        return vertices.size();
    }

    /**
     * Add a <code>Vertex</code> to this <code>Location</code>.
     * @param vertex the <code>Vertex</code> to add
     */
    public void addVertex(Vertex vertex) {
        vertices.add(vertex);
    }

    public void removeVertex(Vertex vertex) {
        vertices.remove(vertex);
    }

    /**
     * @return an ArrayList containing all vertices that occupy this <code>Location</code>.
     */
    public ArrayList<Vertex> getAllVertices() {
        return vertices;
    }

    /**
     * @param index index of the <code>Vertex</code> to get
     * @return the <code>Vertex</code> on that index in the list of vertices that occupy this <code>Location</code>
     */
    public Vertex getVertex(int index) {
        if (getAllVertices().size() == 0) return null;
        return vertices.get(index);
    }

}
